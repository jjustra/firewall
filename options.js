// č

function time () { return parseInt(new Date().getTime()/1000); }

function rndhex () { return Math.floor(4+Math.random()*12).toString(16); }

function err (s) {
//	var prefix='!@#$%^&*'.charAt(Math.floor(Math.random()*8));
	s=time()+' : '+s;
	 document.querySelector("#info").innerText='';
	 document.querySelector("#error").innerText=s;
	 document.querySelector("#error").style.color='#'+rndhex()+'00';
}
function info (s) {
//	var prefix='!@#$%^&*'.charAt(Math.floor(Math.random()*8));
	s=time()+' : '+s;
	 document.querySelector("#info").innerText=s;
	 document.querySelector("#error").innerText='';
	 document.querySelector("#info").style.color='#0'+rndhex()+'0';
}

function check (src,cycle_limit) {
	if (cycle_limit===undefined) cycle_limit=50000;
	var sh=hshlib(src);
	hshlib.loadlib(sh,'std');
	hshlib.loadlib(sh,'firewall_dummy');
	var cnt=0,res,exc=false;
	while (cnt<cycle_limit) {
		try {
			res=hshlib.runline(sh);
		} catch (e) {
			exc=e;
			break;
		}
		if (res==-1) {
			break;
		} else if (res==0) {
		} else if (res==1) {
			break;
		} else {
			break;
		}
		cnt++;
		sh.line++;
	}
	return [cnt,res,exc,sh];
}

function saveOptions(e) {
	var stor=browser.storage.sync||browser.storage.local;
	var src=document.querySelector("#script").value;
	
	var res=check(src);
	console.log({
		script: src,
		res:res,
	});
	if (res[0]>=5000) {
	} else if (res[1]<0) {// result
		err('unknown function on line '+ res[3].code[res[3].line][0] +' : '+res[3].code[res[3].line][1].join(' ')+'');
	} else if (res[2]!==false) {// exception
		err('exception in program on line '+ res[3].code[res[3].line][0] +' : '+res[3].code[res[3].line][1].join(' ')+'');
	} else {
		info('program saved successfully')
		stor.set({
			script: src,
		});
		var bg_prom = browser.runtime.getBackgroundPage();
		bg_prom.then(
			function (bg) {
				console.log('bg cfg load :',bg.load(src));
			},
			function () {
				console.log('bg cfg load err');
			}
		);
	}
  e.preventDefault();
}

function restoreOptions() {
  var stor=browser.storage.sync||browser.storage.local;
  var gettingItem = stor.get(['script']);
  gettingItem.then((res) => {
	console.log(res);
    document.querySelector("#script").value = res.script || '# write your program here';
  });
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
