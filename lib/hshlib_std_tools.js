(function () {

	var lib=hshlib_std;

	//tools
	lib.hsh_print=function (hsh,a) {
		lib.print(hsh,lib.flat(a.slice(1)));
	};
	lib.hsh_raise=function (hsh,a) {
		a[0]=hsh;
		return lib.raise.apply(window,a);
	};
	lib.hsh_alias=function (hsh,a) {
		var fc_p=__hsh__.function(hsh,a[2]);
		if (fc_p===false) {
			lib.raise(hsh,'unknown function :',a[2]);
			return false;
		}
		__hsh__.function(hsh,a[1],fc_p);
		return true;
	};
	lib.hsh_unfunction=function (hsh,a) {
		for (var i=1;i<a.length;i++)
			delete hsh.function_list[a[i]];
	};
	lib.hsh_loadlib=function (hsh,a) {
		var prefix;
		if (a.length>2) prefix=a[2];
		return __hsh__.loadlib(hsh,a[1],prefix);
	};
	lib.hsh_function_list=function (hsh,a) {
		var result=lib.key_filter(hsh.function_list,a.slice(1));
//		result.sort();
		for (var i in result) lib.print(hsh,result[i]);
	};
	lib.hsh_get_function_list=function (hsh,a) {
		var result=lib.key_filter(hsh.function_list,a.slice(1));
//		result.sort();
		return result;
	};
	lib.hsh_variable_list=function (hsh,a) {
		var result=lib.key_filter(hsh.env,a.slice(1));
//		result.sort();
		for (var i in result) lib.print(hsh,result[i]);
	};
	lib.hsh_get_variable_list=function (hsh,a) {
		var result=lib.key_filter(hsh.env,a.slice(1));
//		result.sort();
		return result;
	};
	lib.hsh_data=function (hsh,a) {
		var lni=lib.label(hsh,a[1]);
		if (lni<0) return lib.raise(hsh,'label "',a[1],'" 404');
		var b_translate=false;
		if (a.length>2) b_translate=lib.cast_bool(a[2]);
		var data=[];
		while (lni<hsh.code.length) {
			lni++;
			if (hsh.code[lni][1].length && hsh.code[lni][1][0]==a[1]) break;
			var line= hsh.code[lni][1];
			if ( b_translate && line.length>0) {
				for (var i in line) line[i]=hshlib.token(hsh,line[i]);
			}
			data.push( line.join(' ') );
		}
		return data.join('\n');
	};

})();