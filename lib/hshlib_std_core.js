(function () {

	var lib=hshlib_std;

	lib.n92clist='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ!"#$%&\'()+,-./:<=>?@[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~';
	
//
// LIB
//
	// tools
	lib.clear=function (hsh) {
		hsh.env['___hshlib_std_return_stack']=[];
		hsh.env['___hshlib_std_stdout']=[];
		hsh.env['___hshlib_std_exception_raised']=0;
//		hsh.env['___hshlib_std_']=[];
//		hsh.env['___hshlib_std_']=[];
	};
	lib.strval=function (o,d) {
		if (o===true) return 'true';
		if (o===false) return 'false';
		if (o===null) return 'null';
		if (o===undefined) return 'null';
		if (typeof(o)==='string') return o;
		
		if (d===undefined) d=1;
		
		var res='???';
		var prefix=' '.repeat(d);
		
		if (typeof(o)==='object') {
			res=['array('];
			for (var i in o)
				res.push(prefix+lib.strval(i,d+1)+' => '+lib.strval(o[i],d+1));
			res.push('),');
			res=res.join('\n');
		} else if (typeof(o)==='number') res=o.toString();
		
		return res;
	}
	lib.flat=function(a,force_strval) {
		if (force_strval===undefined) force_strval=false;
		if (a.length==1) {
			if (force_strval) return lib.strval(a[0]);
			return a[0];
		}
		for (var i in a) a[i]=lib.strval(a[i]);
		return a.join(' ');
	};
	lib.key_filter=function (list,s_a) {
		var result=[];
		for (var k in list) {
			if (s_a.length>0) {
				var not_in=false;
				for (var i in s_a)
					if (k.indexOf(s_a[i])<0) {
						not_in=true;
						break;
					}
				if (not_in) continue;
			}
			result.push(k);
		}
		return result;
	};
	lib.value_filter=function (list,s_a) {
		var result=[];
		for (var k in list) {
			if (s_a.length>0) {
				var not_in=false;
				for (var i in s_a)
					if (list[k].indexOf(s_a[i])<0) {
						not_in=true;
						break;
					}
				if (not_in) continue;
			}
			result.push(list[k]);
		}
		return result;
	};
	lib.print=function(hsh) {
		var a=[];
		for (var i=1;i<arguments.length;i++) a.push(arguments[i]);
		hsh.env['___hshlib_std_stdout'].push( lib.flat(a,1).toString() );
	};
	lib.stdout=function (hsh) {
		var stdout=hsh.env['___hshlib_std_stdout'];//.join('\n');
		hsh.env['___hshlib_std_stdout']=[];
		return stdout;
	};
	lib.raise=function (hsh) {
		var a=[];
		for (var i=1;i<arguments.length;i++) a.push(arguments[i]);
		var line='?';
		var code='?';
		if (hsh===undefined) {
			alert(['from undefined hsh\nException raised:\n',lib.flat(a),'\non line',line,'-> code:',code,'\n'].join(' '));
		} else {
			if (hsh.line<hsh.code.length) {
				line= hsh.code[hsh.line][0] ;
				code= hsh.code[hsh.line][1].join(' ') ;
			}
			lib.print(hsh,'\nException raised:\n',lib.flat(a),'\non line',line,'-> code:',code,'\n');
			lib.break(hsh);
			hsh.env['___hshlib_std_exception_raised']++;
		}
		return false;// sugar ;)
	};
	lib.raised=function (hsh,clear) {
		var cnt=hsh.env['___hshlib_std_exception_raised'];
		if(clear!==undefined && clear) hsh.env['___hshlib_std_exception_raised']=0;
		return cnt;
	};
	lib.break=function (hsh) {
		if ( hsh.env['___hshlib_std_return_stack'].length>0 ) {
			hsh.source= hsh.env['___hshlib_std_return_stack'][0][0];
			hsh.code= hsh.env['___hshlib_std_return_stack'][0][1];
		}
		hsh.env['___hshlib_std_return_stack']=[];
		hsh.line=hsh.code.length;
	};
	lib.setargs=function (hsh,arg_name,argv) {
		for (var i=0;i<argv.length;i++) {
			hsh.env['arg'+i]=argv[i];
			if ( i<arg_name.length )
				hsh.env[ arg_name[i] ]=argv[i];
		}
		hsh.env['argc']=argv.length;
		hsh.env['argv']=argv;
	};
	lib.label=function (hsh,lab) {
		for (var i in hsh.code) {
			if (hsh.code[i][1].length==0) continue;
//			if ( hsh.code[i][1][0].charAt(0)!='#' ) continue;
			if ( hsh.code[i][1][0]==lab ) return i;
		}
		return -1;
	};
	lib.jump=function (hsh,lab,save) {
		var lni=lib.label(hsh,lab);
		if (lni<0) {
			lib.raise(hsh,'unknown label :',lab);
		} else {
			if (save) hsh.env['___hshlib_std_return_stack'].push([hsh.source,hsh.code,hsh.line]);
			hsh.line=lni;
		}
		return lni;
	};
	lib.toDigits=function (n,b) {// credits for these 2 functions: Andrej Bauer @ cs.stackexchange.com/questions/10318/
		if (n==0) return [0];
		digits=[];
		while (n>0) {
			digits.unshift(n%b);
			n=Math.floor(n/b);
		}
		return digits;
	};
	lib.fromDigits=function (digits,b) {
		var n=0;
		for (var i in digits) n=b*n+digits[i];
		return n;
	};
	//casts
	lib.cast_num=function (o,force_int) {
		if (o===false) return 0;
		if (o===true) return 1;
		if (o===undefined) return 0;
		if (o===null) return 0;
		var i;
		try{ i=parseInt(o); } catch(e) {return 0;};
		var f=parseFloat(o);
		var n;
		if (o.toString().indexOf('.')>=0)
			n=f;
		else
			n=i;
		if (force_int) n=parseInt(n);
		return n;
	};
	lib.cast_bool=function (o) {
		if (o===true) return true;
		if (o===false) return false;
		if (o===null) return false;
		if (o===undefined) return false;
		if (typeof(o)=='number') {
			if (o==0) return false;
			return true;
		}
		if (o.length!==undefined && o.length==0) return false;
		if (o==='0') return false;
		return true;
	};
	lib.cast_=function (o) {};
	lib.cast_=function (o) {};
	lib.cast_=function (o) {};


})();