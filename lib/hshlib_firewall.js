(function () {

	var lib=function () {};
	
	lib._rule_list=[];

	function regexp (s) {
		try {
			var r=new RegExp(s,'i');
		} catch (e) {
			console.log('regexp err for "',s,'" :',e);
			return new RegExp('.*','i');
		}
		return r;
	}
	function set (ori) {
		var ori_r=regexp (ori);
		var set={
			enabled:true,
			origin:ori_r,
			rules:[],
		};
		lib._rule_list.push(set);
	}

	lib.hsh_origin=function (sh,a) {
		var ori=a.slice(1).join(' ');
		set(ori);
	};
	lib.hsh_block=function (sh,a) {
		var trg=a.slice(1).join(' ');
		var trg_r=regexp (trg);
		var i=lib._rule_list.length-1;
		var rule={ type:'blow', block:true,target:trg_r};
		lib._rule_list[i].rules.push(rule);
	};
	lib.hsh_allow=function (sh,a) {
		var trg=a.slice(1).join(' ');
		var trg_r=regexp (trg);
		var i=lib._rule_list.length-1;
		var rule={ type:'blow', block:false,target:trg_r};
		lib._rule_list[i].rules.push(rule);
	};
	lib.hsh_disable=function (sh,a) {
		var i=lib._rule_list.length-1;
		lib._rule_list[i].enabled=false;
	};
	
	lib.loadlib=function (sh) {
		if (lib._rule_list.length==0) set('.*');
	};

	lib.hsh_requestHeader=function (sh,a) {
		var h=a[1].toLowerCase();
		var v=a.slice(2).join(' ');
		var i=lib._rule_list.length-1;
		var rule={ type:'reqhdr', header:h,value:v};
		lib._rule_list[i].rules.push(rule);
	};
	lib.hsh_responseHeader=function (sh,a) {
		var h=a[1].toLowerCase();
		var v=a.slice(2).join(' ');
		var i=lib._rule_list.length-1;
		var rule={ type:'resphdr', header:h,value:v};
		lib._rule_list[i].rules.push(rule);
	};

	window.hshlib_firewall=lib;

})();