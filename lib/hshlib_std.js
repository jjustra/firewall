(function () {

	var lib=function () {};
	
	//lib.stdout=[];

//
// FUNCTIONS
//
	// creators
	lib.hsh_str=function (hsh,a) {
		return lib.flat(a.slice(1));
	};
	lib.hsh_list=function (hsh,a) {
		return a.slice(1);
	};
	lib.hsh_join=function (hsh,a) {
		if (a.length==3 && typeof(a[2])!='string')
			return a[2].join(a[1]);
		else
			return a.slice(2).join(a[1]);
	};
	lib.hsh_cat=function (hsh,a) {
		if (a.length==3 && typeof(a[2])!='string')
			return a[1].join('');
		else
			return a.slice(1).join('');
	};
	lib.hsh_dict=function (hsh,a) {
		var d={};
		if (a.length==1) return d;
		if (a.length==2) {
			if (typeof(a[1][0])=='string') {
				for (var i in a[1]) d[a[1][i]]='';
			} else {
				for (var i in a[1]) d[a[1][i][0]]=a[1][i][1];
			}
		} else if (a.length==3) {
			var cnt=Math.min(a[1].length,a[2].length);
			for (var i=0;i<cnt;i++) d[a[1][i]]=a[2][i];
		}
	};
	
	lib.hsh_=function (hsh,a) {};

	lib.loadlib=function (hsh) {
		lib.clear(hsh);
	};

	window['hshlib_std']=lib;

})();