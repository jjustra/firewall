(function () {

	var lib=hshlib_std;

	// (en/de)coders
	lib.hsh_b64e=function (hsh,a) {
		return btoa(lib.flat(a.slice(1)));
	};
	lib.hsh_b64d=function (hsh,a) {
		return atob(a[1].replace(/\n/g,''));
	};
	lib.hsh_n92e=function (hsh,a) {
		var n=lib.cast_num(a[1],true).toString().split('').map(function (d) { return parseInt(d); });
		var digits=lib.toDigits(lib.fromDigits(n,10),92);
		var s=[];
		for (var i in digits) s.push(lib.n92clist.charAt(digits[i]));
		return s.join('');
	};
	lib.hsh_n92d=function (hsh,a) {
		var n=a[1].split('').map(function (d) { return lib.n92clist.indexOf(d); });
		var digits=lib.toDigits(lib.fromDigits(n,92),10);
		return digits.join('');
	};
	lib.hsh_btob=function (hsh,a) {
		var b0=lib.cast_num(a[2],true);
		if (b0>92 || b0<2) return lib.raise(hsh,'[btob n b0 b1]\n','base0 must be >=2 and <=92 (',b0,'given)');
		var b1=lib.cast_num(a[3],true);
		if (b1>92 || b1<2) return lib.raise(hsh, '[btob n b0 b1]\n','base1 must be >=2 and <=92 (',b1,'given)');
		var n=a[1].toString().split('').map(function (d) {
			var _n=lib.n92clist.indexOf(d);
			if (_n>=b0) {
				_n=lib.n92clist.indexOf(d.toUpperCase());
				if (_n>=b0) lib.raise(hsh,'digit',d,'out of scope of base',b0);
			}
			return _n;
		});
		var digits=lib.toDigits(lib.fromDigits(n,b0),b1);
		var s=[];
		for (var i in digits) s.push(lib.n92clist.charAt(digits[i]));
		return s.join('');
	};
	lib.hsh_char=function (hsh,a) {
		return String.fromCharCode(lib.cast_num(a[1],true));
	};
	lib.hsh_ascii=function (hsh,a) {
		return a[1].charCodeAt(0);
	};

})();