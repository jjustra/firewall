(function () {

	var o=function (source) {
		var hsh=function () {};
		
		hsh.line=0;
		hsh.retval='';
		hsh.code=[];
		hsh.source='';
		hsh.env={};
		hsh.function_list={};
		
		o.loadlib(hsh,'_hsh');
		
		if (source!==undefined) o.load(hsh,source);
		
		return hsh;
	};
	
		o.load=function ( hsh, source) {
			hsh.code=[];
			hsh.line=0;
			hsh.source=source;
			s=source.replace(/\t/g,' ');
			var s_a=s.split('\n');
			for (var lni in s_a) {
				var line_a=s_a[lni].trim().split(';');
				for (var line_lni in line_a) {
					var token_a=line_a[line_lni].split(' ').filter(function (s) {return s!='';});
					hsh.code.push([lni,token_a]);
				}
			}
		};
		o.token=function ( hsh, s) {
			if(s=='') return s;
			if (s.charAt(0)=='*') {
				if (s=='*') return hsh.retval;
				s=o.token(hsh,s.substr(1));
				if (hsh.env[s]!==undefined) return hsh.env[s];
			}
			return s;
		};
		o.runline=function ( hsh ) {
			if (hsh.line>=hsh.code.length) return 1;
			if ( hsh.code[hsh.line][1].length==0 ) return 0;
			if ( hsh.code[hsh.line][1][0].charAt(0)=='#' ) return 0;
			var token_a=[];
			for (var i in hsh.code[hsh.line][1])
				token_a.push(o.token( hsh, hsh.code[hsh.line][1][i] ));
			var fc=token_a[0];
			var fc_p=o.function( hsh, fc);
			if ( fc_p===undefined ) return -1;
			hsh.retval=window[fc_p[0]][fc_p[1]](hsh,token_a);
			return 0;
		};
		o.function=function ( hsh, fc_name,fc_p) {
			if (fc_p!==undefined) hsh.function_list[fc_name]=fc_p;
			return hsh.function_list[fc_name];
		};
		o.loadlib=function ( hsh, lib_name,prefix) {
			lib_name='hshlib_'+lib_name;
			if (window[lib_name]===undefined) return false;
			if (prefix===undefined) prefix='';
			for (var fc in window[lib_name]) {
				if (fc.indexOf('hsh_')!=0) continue;
				var fc_name=fc.substr(4);
				o.function( hsh, prefix+fc_name,[lib_name,fc,fc_name]);
			}
			if (window[lib_name].loadlib!==undefined) window[lib_name].loadlib(hsh);
			return true;
		};
		
		o.hsh_loadlib=function (sh,a) {
			return o.loadlib(sh,a[1],a[2]);
		};
	
	window['hshlib']=o;
	window['__hshlib__']=o;
	window['hsh']=o;
	window['__hsh__']=o;
	window['hshlib__hsh']=o;

})();