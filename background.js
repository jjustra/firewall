// č

var rule_list=[];

function load (src,cycle_limit) {
	if (cycle_limit===undefined) cycle_limit=50000;
	hshlib_firewall._rule_list=[];
	var sh=hshlib(src);
	hshlib.loadlib(sh,'std');
	hshlib.loadlib(sh,'firewall');
	var cnt=0,res,exc=0;
	while (cnt<cycle_limit) {
		try {
			res=hshlib.runline(sh);
		} catch (e) {
			exc=1;
			break;
		}
		if (res==-1) {
			break;
		} else if (res==0) {
		} else if (res==1) {
			break;
		} else {
			break;
		}
		cnt++;
		sh.line++;
	}
	if (cnt>=cycle_limit) return false;
	if (res<0) return false;
	if (exc) return false;
	
	rule_list=hshlib_firewall._rule_list;
	return hshlib_firewall._rule_list;
}

function req_b4 (e) {
	if (!e.originUrl) e.originUrl=e.url;
	if (!e.originUrl) return {};
	if (!e.url) return {};
	var block=false;
	for (var j=0;j<rule_list.length;j++) {
		if (!e.originUrl.match( rule_list[j].origin )) continue;
		for (var i=0;i< rule_list[j].rules.length;i++) {
			if (rule_list[j].rules[i].type!='blow') continue;
			if (!e.url.match( rule_list[j].rules[i].target )) continue;
			block=rule_list[j].rules[i].block;
		}
	}
	if (block) {
		console.log('blocked:',e.url);
		return {cancel: true};
	}
	return {cancel: false};
}

function resp_hdr (e) {
	if (!e.originUrl) e.originUrl=e.url;
	for (var j=0;j<rule_list.length;j++) {
		if (!e.originUrl.match( rule_list[j].origin )) continue;
		var rules=[];
		for (var i=0;i< rule_list[j].rules.length;i++) {
			if (rule_list[j].rules[i].type!='resphdr') continue;
			rules.push(rule_list[j].rules[i]);
		}
		var rem=[];
		for ( var hi=0;hi<e.responseHeaders.length;hi++) {
			for (var i=0;i< rules.length;i++) {
				if (e.responseHeaders[hi].name.toLowerCase()==rules[i].header) {
					if (!rules[i].value && rem.indexOf(hi)<0)
						rem.push(hi);
					else
						e.responseHeaders[hi].value= rules[i].value;
					console.log('resp_hdr:',e.url,e.responseHeaders[hi]);
				}
			}
		}
		if (rem.length) for (i=rem.length-1;i>=0;i--) e.responseHeaders.splice(rem[i],1);
	}
	return {responseHeaders:e.responseHeaders};
}
function req_hdr (e) {
	if (!e.originUrl) e.originUrl=e.url;
	for (var j=0;j<rule_list.length;j++) {
		if (!e.originUrl.match( rule_list[j].origin )) continue;
		var rules=[];
		for (var i=0;i< rule_list[j].rules.length;i++) {
			if (rule_list[j].rules[i].type!='reqhdr') continue;
			rules.push(rule_list[j].rules[i]);
		}
		var rem=[];
		for ( var hi=0;hi<e.requestHeaders.length;hi++) {
			for (var i=0;i< rules.length;i++) {
				if (e.requestHeaders[hi].name.toLowerCase()==rules[i].header) {
					if (!rules[i].value && rem.indexOf(hi)<0)
						rem.push(hi);
					else
						e.requestHeaders[hi].value= rules[i].value;
					console.log('req_hdr:',e.url,e.requestHeaders[hi]);
				}
			}
		}
		if (rem.length) for (i=rem.length-1;i>=0;i--) e.requestHeaders.splice(rem[i],1);
	}
	return {requestHeaders:e.requestHeaders};
}

browser.webRequest.onBeforeRequest.addListener(
	req_b4,
	{'urls':['<all_urls>']},
	['blocking']
);
browser.webRequest.onHeadersReceived.addListener(
	resp_hdr,
	{'urls':['<all_urls>']},
	['blocking','responseHeaders']
);
browser.webRequest.onBeforeSendHeaders.addListener(
	req_hdr,
	{'urls':['<all_urls>']},
	['blocking','requestHeaders']
);

var stor=browser.storage.sync||browser.storage.local;
var gettingItem = stor.get(['script']);
gettingItem.then((res) => {
	console.log('loading program :',load(res.script));
});
